import Vue from 'vue'
import VueRouter from 'vue-router'
import Profile from '@/components/profile/profile.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    name: 'profile',
    component: Profile
  }
]

const router = new VueRouter({
  routes
})

export default router
