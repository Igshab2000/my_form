import arrayFields from './arrayField'

export default function messageOutput(objectValidation) {
    if(!objectValidation.required) {
        return 'Поле обязательное для заполнения';
    } else if(arrayFields[objectValidation.index].type === 'text' && objectValidation.error) {
        return 'Поле должно содержать меньше 365 символов';
    } else if(arrayFields[objectValidation.index].id === 'age' && objectValidation.error) {
        return 'Возраст указывается от 0 до 110 лет';
    } else if(arrayFields[objectValidation.index].id === 'size' && objectValidation.error) {
        return 'Рост указывается от 50 до 250 см';
    } else if(arrayFields[objectValidation.index].id === 'weight' && objectValidation.error) {
        return 'Вес указывается от 5 до 280 кг';
    } else if(objectValidation.required && !objectValidation.error) {
        return '';
    }
}