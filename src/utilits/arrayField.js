const arrayFields = [
  {type: 'text', id: 'lastName', placeholder: 'Фамилия', index: 0, message: ''}, 
  {type: 'text', id: 'firstName', placeholder: 'Имя', index: 1, message: ''}, 
  {type: 'text', id: 'patronymic', placeholder: 'Отчество', index: 2, message: ''},
  {type: 'number', id: 'age', placeholder: 'Возраст', index: 3, message: ''},
  {type: 'number', id: 'size', placeholder: 'Рост', index: 4, message: ''},
  {type: 'number', id: 'weight', placeholder: 'Вес', index: 5, message: ''}
];

export default arrayFields;
     
