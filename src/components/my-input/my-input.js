import validationsMixin from '@/mixins/validationsMixin';

export default {
    name: 'my-input',
    props: ['type', 'id', 'placeholder', 'isCheckSubmit', 'message'],
    mixins: [validationsMixin]
}