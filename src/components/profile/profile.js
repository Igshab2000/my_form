import arrayFields from '@/utilits/arrayField'
import messageOutput from '@/utilits/messageOutput'

export default {
    name: 'profile',
    data() {
      return {
        arrayFields,
        isCheckSubmit: false,
      }
    },

    methods: {
      receivedData(textValue) {
        this.arrayFields[textValue.index].message = messageOutput(textValue);
      },

      onSubmit() {
        this.isCheckSubmit = true;
        console.log('submit');
      }
    }
  }