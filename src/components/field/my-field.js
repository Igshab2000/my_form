export default {
    name: 'my-field',
    props: ['type', 'id', 'placeholder', 'index', 'isCheckSubmit', 'message'],
    data() {
        return {
            
        }
    },

    methods: {
        sendDate(value) {
            value.index = this.index;
            this.$emit('receivedData', value);
        }
    }
}