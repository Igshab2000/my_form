import { required, maxLength, between } from 'vuelidate/lib/validators';

export default {
    data() {
        return {
            value: '',
            text: '',
            age: 0,
            size: 0,
            weight: 0,
            error: null
        }
    },
    methods: {
        valuesValidation() {
            if(this.type === 'text') {
                this.text = this.value;
                this.$v.text.$touch();
                this.sendDataParent(this.$v.text);

            } else if(this.type === 'number') {
                
                if(this.id === 'age') {
                    this.age = parseInt(this.value);
                    this.$v.age.$touch();
                    this.sendDataParent(this.$v.age);

                } else if(this.id === 'size') {
                    this.size = parseInt(this.value);
                    this.$v.size.$touch();
                    this.sendDataParent(this.$v.size);

                } else if(this.id === 'weight') {
                    this.weight = parseInt(this.value);
                    this.$v.weight.$touch();
                    this.sendDataParent(this.$v.weight);
                }
                
            }
        },

        sendDataParent(value) {
            this.$emit('textValue', {
                value: value.$model,
                required: value.required,
                error: value.$error
            });
        }

    },

    validations: {
        text: {
            required,
            maxLength: maxLength(365),
        },

        age: {
            required,
            between: between(0, 110)
        },

        size: {
            required,
            between: between(50, 250)
        },

        weight: {
            required,
            between: between(5, 280)
        }
    }
}