import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import MyInput from '@/components/my-input/my-input.vue'
import MyField from '@/components/field/my-field.vue'
import Message from '@/components/message/message.vue'


Vue.config.productionTip = false
Vue.use(Vuelidate);
Vue.component('my-input', MyInput);
Vue.component('my-field', MyField);
Vue.component('message', Message);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
